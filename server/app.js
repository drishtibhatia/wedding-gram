var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var Sequelize = require("sequelize");
var request = require("request");

const MYSQL_USERNAME = "root";
const MYSQL_PASSWORD = "krishna1234";

var conn = new Sequelize(
    'weddings',
    MYSQL_USERNAME,
    MYSQL_PASSWORD, {
        host: 'localhost',
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

var guests = require('./models/guest') (conn,Sequelize);
//var User  = require('./models/user') (conn,Sequelize);
var Comment = require('./models/comment')(conn, Sequelize); // import own App modules

const NODE_PORT = process.env.NODE_PORT || 3000;
const CLIENT_FOLDER = path.join(__dirname + '/../client');
const MSG_FOLDER = path.join(CLIENT_FOLDER + '/assets/messages');

var app = express();
app.use(express.static(CLIENT_FOLDER));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : false}));

app.get("/api/email", function(req, resp) {
    var email = req.query.email;
    var name = req.query.name;
    console.log("Main.js Email: %s , Name : %s ", email,name);
    request({
        url: "https://script.google.com/macros/s/AKfycbyOdYIqP-6D94hlieXWfz_kgSwNH3sCttkndKogfdxYNVUlau7P/exec",
        qs: { 
            email: email,
            name : name
        },
        method: "GET",
        headers: {'Access-Control-Allow-Origin': '*'}
    }, function(error, response, body) {
        if (error) {
            console.log(response.statusCode, body);
            resp.status(response.statusCode);
        }
        else{
            console.log(resp.statusCode, body);
            resp.status(200);
        }
        resp.end();
    })
});

app.post("/api/guests", function(req, res) {
    console.log("Insert Guest");
    console.log(req.body);
    console.log(req.body.guest.email);
    console.log(req.body.guest.name);
    guests
        .create({
            guest_email: req.body.guest.email,
            guest_name: req.body.guest.name
        }).then(function(guest) {
            res.status(200).json(guest);
        }).catch(function(err) {
            console.log("Create Error");
            res.status(500).json(err);
        });
});

app.get("/api/guests", function(req, res) {
    console.log("list Guest");

    conn.query("SELECT * FROM guests", { type: conn.QueryTypes.SELECT })
        .then(function(results) {
            console.log(results);
            res.status(202);
            res.json(results);
        })
        .catch(function(err) {
            console.log(err);
        });

});

app.get("/api/attendance", function(req, res) {
    console.log("mark attendance");
    console.log(req.query.attendance);
    console.log(req.query.guest_email);
    console.log(req.query);
    guests
        .update({
            attendance: req.query.attendance,
        }, { where: { guest_email: req.query.guest_email } }).then(function(guest) {
            console.log("update done");
            console.log(guest);
            res.status(200).send(guest);
        }).catch(function(err) {
            console.log("Create Error");
            res.status(500).json(err);
        });
});

app.delete("/api/delguest/:guest_email", function(req, res) {
    var where = {};
    where.guest_email = req.params.guest_email;
    console.log("Delete Email: ", req.params.guest_email);
    // res.status(200).send("ok");
    guests
        .destroy({
            where: where
        })
        .then(function(result) {
            if (result == "1")
                res.json({ success: true });
            else
                res.json({ success: false });
        })
        .catch(function(err) {
            console.log("-- DELETE /api/managers/:dept_no/:emp_no catch(): \n" + JSON.stringify(err));
        });
});

// hb start
// create a single comment into MySQL
app.post("/comments", function(req, res){
    console.log("APP SVR POST >> %s", req); // , JSON.stringify(req.body.cmt));

    if (req.body.cmt.c_couple == "1") {
        Comment
            .create({
                c_date: req.body.cmt.c_date, 
                c_user: req.body.cmt.c_user, 
                c_image: req.body.cmt.c_image, 
                c_text: req.body.cmt.c_text, 
                c_couple: req.body.cmt.c_couple 
            })
            .then(function(commentrow){
                console.log("APP SVR POST >> SUCCESS %s", commentrow);
                res
                    .status(200)
                    .json(commentrow);
            })
            .catch(function(){
                console.log("APP SVR POST >> ERROR %s", err);
                res
                    .status(500)
                    .json(err);
            })
    };
    
    if (req.body.cmt.c_couple == "2") {
        Comment2
            .create({
                c_date: req.body.cmt.c_date, 
                c_user: req.body.cmt.c_user, 
                c_image: req.body.cmt.c_image, 
                c_text: req.body.cmt.c_text, 
                c_couple: req.body.cmt.c_couple 
            })
            .then(function(commentrow){
                console.log("APP SVR POST >> SUCCESS %s", commentrow);
                res
                    .status(200)
                    .json(commentrow);
            })
            .catch(function(){
                console.log("APP SVR POST >> ERROR %s", err);
                res
                    .status(500)
                    .json(err);
            })
    };

    if (req.body.cmt.c_couple == "3") {
        Comment3
            .create({
                c_date: req.body.cmt.c_date, 
                c_user: req.body.cmt.c_user, 
                c_image: req.body.cmt.c_image, 
                c_text: req.body.cmt.c_text, 
                c_couple: req.body.cmt.c_couple 
            })
            .then(function(commentrow){
                console.log("APP SVR POST >> SUCCESS %s", commentrow);
                res
                    .status(200)
                    .json(commentrow);
            })
            .catch(function(){
                console.log("APP SVR POST >> ERROR %s", err);
                res
                    .status(500)
                    .json(err);
            })
        };
    
});

// route path - retrieve/show all comments
 app.get("/comments", function(req, res){ 
     console.log("APP SVR req.query.schStr: ", req.query.searchString);
     var searchString  = req.query.searchString;
     
     if (searchString == '1') { 
        Comment
            .findAll({ // returns an Array of Objects
            })
            .then(function(commentrow){
                console.log("APP SVR findAll() 1 >> ", commentrow);         
                res
                    .status(200)
                    .json(commentrow);
            })
            .catch(function(err){
                res
                    .status(500)
                    .json(err);
            });   
     }

     if (searchString == '2') {
        Comment2
            .findAll({ // returns an Array of Objects
            })
            .then(function(commentrow){
                console.log("APP SVR findAll() 2 >> ", commentrow);          
                res
                    .status(200)
                    .json(commentrow);
            })
            .catch(function(err){
                res
                    .status(500)
                    .json(err);
            });   
     }

     if (searchString == '3') {
        Comment3
            .findAll({ // returns an Array of Objects
            })
            .then(function(commentrow){
                console.log("APP SVR findAll() 3 >> ", commentrow);          
                res
                    .status(200)
                    .json(commentrow);
            })
            .catch(function(err){
                res
                    .status(500)
                    .json(err);
            });   
     }
      
 });

// hb end

/*
// route path - insert department record
app.post("/api/guest", function(req, res){
    Guest
        .create({
            guest_email: req.body.guest.email,
            guest_name: req.body.guest.name 
        }).then(function(guest){
            res.status(200).json(guest);
        }).catch(function(err){
            res.status(500).json(err);
        });
});

//register user to db 
app.post("/api/register", function(req, res){
    console.log("inside server POST, Adding new user to db ");
    console.log(req.body);
    User
        .create({
            email: req.body.user.email,
            username: req.body.user.usernames ,
            password : req.body.user.passwords,
            user_type : req.body.user.user_type
        }).then(function(user){
            console.log(user);
            res.status(200).json(user);
        }).catch(function(err){
            res.status(500).json(err);
        });
});
*/

app.use(function (req, res) {
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});
/* start of GET PUT POST calls*/

app.use(function (err, req, res, next) {
    res.status(501).sendFile(path.join(MSG_FOLDER + '/501.html'));
});

app.listen(NODE_PORT, function () {
    console.log("Server running at http://localhost:" + NODE_PORT);
});