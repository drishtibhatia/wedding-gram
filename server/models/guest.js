module.exports = function(conn, Sequelize) {
    var guests = conn.define('guests', {
            guest_email: {
                type: Sequelize.STRING,
                primaryKey: true,
                allowNull: false,
                validate: {
                    isEmail: true, // Back end email validation :)
                }
            },
            guest_name: {
                type: Sequelize.STRING,
                allowNull: false,

            },
            tableNo: {
                type: Sequelize.INTEGER,
                allowNull: true
            },
            attendance: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
                allowNull: true
            }
        },

        // conn.sync({
        //     force: true
        // })
        // .then(function() {
        //     console.log('Item table created successfully');
        // }),

        {
            tableName: "guests",
            timestamps: false,

        });
    return guests;
};