//database : weddings
//Table Name : user
//user_id , email, username , password , user_type
module.exports = function (conn, Sequelize){
    var User =  conn.define('user', {
        user_id:{
            type:Sequelize.INTEGER(5),
            primaryKey:true,
            allowNull:false,
            autoIncrement:true  
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false,
            unique : true
        },
        username: {
            type: Sequelize.STRING,
            allowNull: false
        },
        password : {
            type:Sequelize.STRING,
            allowNull:false
        },
        user_type : {
            type : Sequelize.ENUM('guest','couple'),
            allowNull:false
        }
    }, {
        tableName:'user',
        //freezeTable : true,
        timestamps: false
    });
    return User;
};