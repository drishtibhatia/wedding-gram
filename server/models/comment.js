// comment.js
// map to MySQL data model - to be required into server App JS

module.exports = function(conn2, Sequelize) {
    var Comments = conn2.define("commenttbl", {
        c_date: {
            type: Sequelize.DATE,
            allowNull: false,
            primaryKey: true
        },
        c_user: {
            type: Sequelize.STRING,
            allowNull: true
        },
        c_image: {
            type: Sequelize.STRING,
            allowNull: true,
            primaryKey: true
        },
        c_text: {
            type: Sequelize.STRING,
            allowNull: true
        },
        c_couple: {
            type: Sequelize.STRING,
            allowNull: true
        },
    }, {
        tableName: "commenttbl",
        timestamps: false
    });
    console.log("Models-Comment >> ", Comments);
    return Comments;
};