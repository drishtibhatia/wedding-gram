(function(){
    angular
        .module("WMS")
        .controller("LoginCtrl", LoginCtrl);

LoginCtrl.$inject = ["$state","WedService"];

function LoginCtrl($state,WedService){
    var vm = this;

    vm.user  = { 
            email:"", 
            password:""
        };
        
    vm.status = "";
    vm.login = login;

    function login(){

        WedService.login(vm.user)
            .then(function(result){
                vm.status = "Login Sucessfull";
                console.log("result " + JSON.stringify(result));
                //$window.location.assign('/app/gallery/gallery.html'); 
                $state.go("gallery");
            })
            .catch(function(err){
                vm.status = "Email or Password in correct";
                //$state.go("SignIn");
            });
    }//end of login()
} //end of LoginCtrl()
    
})();