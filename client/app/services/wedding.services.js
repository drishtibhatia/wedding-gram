(function() {
    angular
        .module("WMS")
        .service("WedService", WedService);

    WedService.$inject = ['$http'];

    function WedService($http) {
        var service = this;

        service.insertGuest = insertGuest;
        service.retrieveGuest = retrieveGuest;
        service.markAttendance = markAttendance;
        service.sendEmail = sendEmail; ///defined email function
        //service.login = login;
        //service.register = register;
        service.deleteGuest = deleteGuest;

        ///email function added 
        function sendEmail(emailParam,nameParam){
            console.log("MailerSvc:  Mail Service Email: %s and Name : %s",emailParam ,nameParam); 
            var Indata = {
                    email:emailParam,
                    name:nameParam
                };
            return ($http.get("api/email", {
				params: Indata
            }));    
         };
        /////end of email function ()
        function insertGuest(guest) {
            console.log("inside service");
            return $http({
                method: 'POST',
                url: 'api/guests',
                data: {
                    guest: guest
                }
            });
        }

        function retrieveGuest() {
            console.log("inside retrieveGuest");
            return $http({
                method: 'GET',
                url: 'api/guests'


            });
        }

        function markAttendance(guest_email, attendance) {
            console.log("Email: ", guest_email);
            console.log("Attendance: ", attendance);
            console.log("inside markAttendance");
            return $http({
                method: 'GET',
                url: 'api/attendance',
                params: {
                    guest_email: guest_email,
                    attendance: attendance
                }

            });
        }

        function deleteGuest(guest_email) {
            console.log("inside wedding service deleteGuest")
            console.log("guest del", guest_email);
            return $http({
                method: 'DELETE',
                url: 'api/delguest/' + guest_email
            });

        }
/*
//commented the login and registeration process
        function login(userObj) {
            console.log(userObj);
            return $http({
                method: "GET",
                url: 'api/loginUser',
                params: {
                    'email': userObj.email,
                    'password': userObj.password
                }
            });
        }

        function register(userObj) {
            console.log("inside services", userObj);
            return $http({
                method: "POST",
                url: 'api/register',
                data: { user: userObj }
            });
        }
*/
    }
})();
                            