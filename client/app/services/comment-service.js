// d7 Comment-handler.js - for Service injection
(function() { // no Controller needed if UI not involved

 angular
        .module("WMS")
        .service("CommentSvc", CommentSvc);

    CommentSvc.$inject = ['$http'];

   function CommentSvc($http) {

        var commentSvc = this;

        commentSvc.loadImage = function() {
            console.log("Comment SVC: loadImage()...");
            return ($http.get("/loadImage")
                .then(function(imgResult) {
                    console.log("Comment SVC: >>> Image: %s", imgResult);
                    return (imgResult.data);
                }));
        }

        // commentSvc.placeComment = placeComment;

        commentSvc.placeComment = function(commentDetail) {
            console.log("Comment SVC: placeComment()---");
            console.log("Comment SVC: commentDetail: ", commentDetail);
            // return ($http.get("/placeComment", {
            //     params: commentDetail  
            // }));
            return $http({
                method: 'POST',
                url: 'comments',
                data: { cmt: commentDetail } // body of response messageS 
            });
        }

        commentSvc.showComment = function() {
            console.log("Comment SVC: showComment()...");

            // DEBUG
            // var commentDetail = {
            //     date: new Date(),  // new Date(commentCtrl.timestamp), // date
            //     user: commentCtrl.user,
            //     image: "777.jpg",  // commentCtrl.img,
            //     text: commentCtrl.comment // txt
            // }
            var commentDetail = "hello";

            console.log("Comment SVC commentDetail: ", commentDetail);
            
            // return ($http.get("/showComment")
            //     .then(function(result) {
            //         console.log("Comment SVC: >>> Results: %s", result);
            //         return (result.data);
            // }));
            return $http({
                method: 'GET',
                url: 'comments',
                params : { 'searchString': c_couple = '1' }
                // data: { cmt: commentDetail } // body of response msg to control GET's content
            });
        }
    };

   
}) ();





