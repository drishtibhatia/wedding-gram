(function () {
    angular
        .module("WMS")
        .config(uirouterAppConfig);
    uirouterAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function uirouterAppConfig($stateProvider,$urlRouterProvider){
        $stateProvider
            .state('user',{
                url: '/user',
                templateUrl:"app/user/user.html"
            })
            .state('guest',{
                url: '/guest',
                templateUrl: "app/guest/guest.html"
            })
            .state('couple',{
                url: '/couple',
                templateUrl: "app/couple/couple.html"
            })
            .state("gallery1", {
                url: "/gallery1",
                templateUrl: "app/gallery/gallery1.html"
            })
            .state("rsvp", {
                url: "/rsvp",
                parent: 'gallery1',
                templateUrl: "app/registrationGuest/registerGuest.html"
            })
             .state("list", {
                url: "/list",
                parent: 'couple',
                templateUrl: "app/list/list.html",
            })

        $urlRouterProvider.otherwise("/user");
    
    }

})();