// d7-flower : public/index.html  public/index.js

// (functio() {}) ();  IIFE

(function() {
      angular
        .module("WMS")
        .controller("CommentCtrl", CommentCtrl);

    CommentCtrl.$inject=['CommentSvc']; 
    
// every Angular application must have at least one Controller
  function CommentCtrl(CommentSvc) { // link FlowerSvc into FlowerCtrl
        commentCtrl = this;

        commentCtrl.user = ""; // username
        commentCtrl.timestamp = "";
        commentCtrl.comment = "";
        commentCtrl.img = "";
        commentCtrl.commentArray = [];


        commentCtrl.addArr = function() {
                    console.log("Comment CTRL: -> pushing into Array...");
                    commentCtrl.commentArray.push(commentCtrl); // user-timestamp-comment
                    console.log("Comment CTRL: Array>>> %s", commentCtrl.commentArray);
                    console.log("Comment CTRL: type-> %s", commentCtrl);
        }

    var initForm= function(commentCtrl) {
        commentCtrl.user = ""; // username
        commentCtrl.date = "";
        commentCtrl.image = "";
        commentCtrl.comment = "";
        
    }

    var commentQueryObject = function(commentCtrl) {
        return({
            c_date: new Date(),  // new Date(commentCtrl.timestamp), // date
            c_user: commentCtrl.user,
            c_image: "",  // commentCtrl.img,
            c_text: commentCtrl.comment, // txt
            c_couple: "1"            
        })
            // user: commentCtrl.user,
            // timestamp: commentCtrl.timestamp,
            // comment: commentCtrl.comment
            
            
            // id: commentCtrl.id,
            // user: commentCtrl.cmt.user,
            // timestamp: commentCtrl.cmt.timestamp,
            // comment: commentCtrl.cmt.comment,

         // hb
    }

    var imgArray = [
        "/images/RV_Wedding_Photography_Singapore034.jpg",
        "/images/Wedding06-main.jpg",
        "/images/Wedding4.jpg",
        "/images/CT7TjbUVAAAK9jH.jpg",
        "/images/jewish-wedding-image-for-blog-e1440167907886.jpg",
        "/images/wedding-1183271_960_720.jpg",
        "/images/16333667-A-young-couple-in-love-bride-and-groom-wedding-day-in-summer-Enjoy-a-moment-of-happiness-and-love-in-Stock-Photo.jpg"
];

    initForm(commentCtrl);
    
    // load image from server
    commentCtrl.loadImage = function() {
        console.log("Comment CTRL: inside commentCtrl.loadImage...");
        commentCtrl.images = [];
        CommentSvc.loadImage()
                .then(function(allImages) {
                    commentCtrl.images = allImages;
                    console.log("Comment CTRL: ", allImages);
        })            
    }

    // using CommentSvc ($http) to send data to server
    commentCtrl.placeComment = function() {
        console.log("Comment CTRL: inside commentCtrl.placeComment...");
        // commentCtrl.showComment();

        commentCtrl.date = (new Date()).toString(); // set time to comment
        // commentCtrl.user & commentCtrl.comment & commentCtrl.image entered at HTML
        commentCtrl.addArr(); // push object into array
        CommentSvc.placeComment(commentQueryObject(commentCtrl))
            .then(function() {
                commentCtrl.showComment();
                initForm(commentCtrl);
            });
    }

    commentCtrl.showComment = function() {
        console.log("Comment CTRL: inside commentCtrl.showComment...");
        commentCtrl.comments = [];
        CommentSvc.showComment()
                .then(function(allComments) { // returns from MySQL an ArrayObj of Objects
                    // show allComments in reverse order (LIFO)
                    var temp = allComments.data;
                    console.log("Comment CTRL >> %s %s", allComments, temp);
                    allComments.data = temp.reverse();
                    commentCtrl.comments = allComments.data;
                    console.log("Comment CTRL: ", allComments.data);
                    console.log("Comment CTRL commentCtrl.comments: ", commentCtrl.comments);

        })            
    }
}    

   
}) ();








